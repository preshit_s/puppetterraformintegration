provider "aws" {
    region = "us-east-1"
}

resource "aws_instance" "my_instance" {
    ami = "ami-0440d3b780d96b29d"
    instance_type = "t2.micro"
    key_name = "myec2key"

    tags = {
      Name = "my_instance"
    }
}

resource "aws_s3_bucket" "preshitsbucket" {
    bucket = "preshitsbucket"
}