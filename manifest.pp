
# Define node 'default' to apply configuration to all nodes
node 'default' {

  # Define an exec resource to initialize Terraform
  exec { 'terraform_init':
    command => 'C:\Program Files\terraform\terraform.exe init', # Path to your terraform executable
    cwd     => 'C:\Program Files\terraform', # Directory where Terraform configuration files reside
  }

  # Define an exec resource to validate Terraform configuration
  exec { 'terraform_validate':
    command => 'C:\Program Files\terraform\terraform.exe validate',
    cwd     => 'C:\Program Files\terraform',
    require => Exec['terraform_init'], # Ensure 'terraform_init' is executed first
  }

  # Define an exec resource to plan Terraform changes
  exec { 'terraform_plan':
    command => 'C:\Program Files\terraform\terraform.exe plan',
    cwd     => 'C:\Program Files\terraform',
    require => Exec['terraform_validate'], # Ensure 'terraform_validate' is executed first
  }

  # Define an exec resource to apply Terraform changes
  exec { 'terraform_apply':
    command => 'C:\Program Files\terraform\terraform.exe apply -auto-approve', # Auto-approve flag to avoid manual confirmation
    cwd     => 'C:\Program Files\terraform',
    require => Exec['terraform_plan'], # Ensure 'terraform_plan' is executed first
  }

  # Define an exec resource to destroy Terraform-managed infrastructure
  exec { 'terraform_destroy':
    command => 'C:\Program Files\terraform\terraform.exe destroy -auto-approve', # Auto-approve flag to avoid manual confirmation
    cwd     => 'C:\Program Files\terraform',
  }
}
